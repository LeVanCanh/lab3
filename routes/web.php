<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('blade', function(){
    $drinks = array('Rượu chuối hột','Nếp Mới','Vodka','Spirytus');
    return view('page', array('name'=> 'Lê Văn Cảnh','day'=> 'Thursday','drinks'=> $drinks));
});

// Route::get('/category', function(){
//     $ketqua=DB::select('select * from product_categories');
//     return view('category',['ketqua'=>$ketqua]);
// });

Route::get('/category', 'CategoryController@index');


