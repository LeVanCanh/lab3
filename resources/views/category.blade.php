
@section('content')

<div class="main">
    <h2>Danh sách danh mục</h2>
    <table width="100%" cellpadding="10px" cellspacing="0" style="border-collapse: collapse">
        <tr bgcolor="#3C3">
            <td>STT</td>
            <td>Mã</td>
            <td>Tên</td>
        </tr>

        @forelse($ketqua as $kq)
            @if($loop->even)
        <tr bgcolor="#CCC">
            @else
        <tr bgcolor="#FFF">
            @endif
            <td>
                {{ $loop->index + 1}}
            </td>
            <td>
                {{ $kq->product_category_id}}    
            </td>
            <td>
                {{ $kq->product_category_name}}
            </td>
        </tr>
        @empty
        KHÔNG CÓ KẾT QUẢ NÀO
        @endforelse
    </table>
</div>