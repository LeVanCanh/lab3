@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')

<p>This is appended to the master sidabar.</p>
@endsection

@section('content')
    <h2>{{$name}}</h2>
    <p>This is my body content</p>
    @if ($day == 'Thursday')
        <p>Time to make money</p>
    @else
        <p>Time to party</p>
    @endif

    <h2>Foreach Loop</h2>
    @foreach ($drinks as $drinks)
    {{$drinks}} <br>
    @endforeach

    <h2>Execte PHP Funtion</h2>
    <p>The date is {{date(' D M, Y')}}</p>
@endsection